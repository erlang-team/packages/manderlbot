manderlbot (0.9.3-3) unstable; urgency=medium

  * Fix building the source package after successful build (closes: #1045852).
  * Bump standards version to 4.7.0.

 -- Sergei Golovan <sgolovan@debian.org>  Thu, 20 Feb 2025 14:18:28 +0300

manderlbot (0.9.3-2) unstable; urgency=medium

  * Incorporate the NMU by Holger Levsen.
  * Fix the debian/watch uscan control file.
  * Add patch by Vagrant Cascadian which fixes erl path for usrmerge systems
    (closes: #1037296).
  * Bump debhelper compatibility level to 13.
  * Bump standards version to 4.6.2.

 -- Sergei Golovan <sgolovan@debian.org>  Mon, 10 Jul 2023 09:18:50 +0300

manderlbot (0.9.3-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sat, 02 Jan 2021 02:20:20 +0100

manderlbot (0.9.3-1) unstable; urgency=medium

  * New upstream release.
  * Refresh patches, remove no longer necessary ones.
  * Add debian/watch to track future releases.

 -- Sergei Golovan <sgolovan@debian.org>  Thu, 12 Apr 2018 13:29:41 +0300

manderlbot (0.9.2-20) unstable; urgency=medium

  * Convert source files to UTF-8 since this encoding is a default one for
    Erlang sources now.
  * Replace calls to the deprecated random module by calls to the rand one.
  * Refresh and annotate patches.
  * Add Salsa VCS headers.
  * Bump debhelper compatibility level to 10.
  * Bump standards version to 4.1.3.

 -- Sergei Golovan <sgolovan@debian.org>  Tue, 03 Apr 2018 07:50:44 +0300

manderlbot (0.9.2-19) unstable; urgency=low

  * Fixed options in manpage.
  * Removed % from additional CSS to please hevea (closes: #753225).
  * Bumped standards version to 3.9.5.

 -- Sergei Golovan <sgolovan@debian.org>  Mon, 07 Jul 2014 12:38:21 +0400

manderlbot (0.9.2-18) unstable; urgency=low

  * Switched to 3.0 (quilt) source format.
  * Bumped debhelper compatibility version to 8.
  * Bumped standards version to 3.9.4.
  * Added ${misc:Depends} variable to the binary package dependencies list.
  * Removed an article from the package short description.

 -- Sergei Golovan <sgolovan@debian.org>  Thu, 15 Aug 2013 21:34:09 +0400

manderlbot (0.9.2-17) unstable; urgency=low

  * New maintainer's email address pkg-erlang-devel@lists.alioth.debian.org
    mailing list.
  * Fixed two small bugs in bloto and fortune behaviors.
  * Use ${erlang:Depends} subst variable which expands into automatically
    calculated dependencies by erlang-depends.
  * Added debian/README.source file with a reference to
    /usr/share/doc/quilt/README.source.
  * Bumped standards version to 3.8.3.

 -- Sergei Golovan <sgolovan@debian.org>  Fri, 02 Oct 2009 00:29:28 +0400

manderlbot (0.9.2-16) unstable; urgency=low

  * Removed Torsten Werner from the uploaders list as per his request.
  * Loosened runtime dependencies to erlang-inets and erlang-xmerl to match
    new Erlang packages structure.
  * Added cm-super to build-dependencies to use Type1 fonts in PDF
    documentation.
  * Fixed page size in PDF documentation to fit paper size.
  * Removed documentation in intermediate LaTeX format from the binary
    package.
  * Bumped standards version to 3.8.2.

 -- Sergei Golovan <sgolovan@debian.org>  Mon, 29 Jun 2009 10:19:31 +0400

manderlbot (0.9.2-15) unstable; urgency=low

  * Protected quilt calls in debian/rules to make the source package
    convertible to 3.0 (quilt) format (closes: #484911).
  * Fixed default config file (removed extra spaces from its elements
    character data).
  * Removed obsolete guards and switched to re from regexp module.
  * Added build-dependency on erlang-dev (>= 1:13.b) to make transition to
    Erlang R13B easier.
  * Bumped standards version to 3.8.1.

 -- Sergei Golovan <sgolovan@debian.org>  Fri, 01 May 2009 20:37:44 +0400

manderlbot (0.9.2-14) unstable; urgency=low

  * Changed startup procedure to make manderlbot starting after erlang
    upgrade.
  * Fixed manderlbot synopsis in manpage.

 -- Sergei Golovan <sgolovan@debian.org>  Tue, 03 Feb 2009 19:33:14 +0300

manderlbot (0.9.2-13) unstable; urgency=low

  * Fixed FTBFS in case when a build directory doesn't follow pattern
    manderlbot-<version> (closes: #483347).

 -- Sergei Golovan <sgolovan@debian.org>  Wed, 28 May 2008 15:43:44 +0400

manderlbot (0.9.2-12) unstable; urgency=low

  * Fixed clean target to work with debhelper 7.0 (made dh_clean the last
    command, so debhelper logs are removed now).
  * Bumped standards version to 3.7.3.
  * Clarified copyright notice.

 -- Sergei Golovan <sgolovan@debian.org>  Sun, 18 May 2008 12:10:15 +0400

manderlbot (0.9.2-11) unstable; urgency=low

  * Adapted manual page source to new more strict docbook2man (closes: #450458).

 -- Sergei Golovan <sgolovan@debian.org>  Wed, 07 Nov 2007 22:12:56 +0300

manderlbot (0.9.2-10) unstable; urgency=low

  * Added Sergei Golovan <sgolovan@debian.org> to uploaders list.
  * Rewritten clean target in debian/rules to ignore only missing Makefile
    error.
  * Made clean-patched target in debian/rules depend on patch-stamp.
  * Bumped debhelper compatibility level to 5.

 -- Sergei Golovan <sgolovan@debian.org>  Thu, 06 Sep 2007 19:25:24 +0400

manderlbot (0.9.2-9) unstable; urgency=low

  * Upload to unstable.

 -- Torsten Werner <twerner@debian.org>  Thu, 17 May 2007 20:36:50 +0400

manderlbot (0.9.2-8) experimental; urgency=low

  [ Torsten Werner ]
  * Remove already fulfilled (ake redundant) Build-Depends.
  * Switched to erlang-depends.
  * Switch to texlive instead of tetex.

  [ Sergei Golovan ]
  * Fixed clean target in debian/rules. (Closes: #424565)
  * Applied quilt for patch management.

 -- Torsten Werner <twerner@debian.org>  Wed, 16 May 2007 21:18:37 +0200

manderlbot (0.9.2-7) unstable; urgency=low

  [ Sergei Golovan ]
  * Removed sources from resulting package. If someone wants to get
    the sources, they are available via apt-get source.

 -- Erlang Packagers <erlang-pkg-devel@lists.berlios.de>  Tue, 19 Sep 2006 19:02:36 +0400

manderlbot (0.9.2-6) unstable; urgency=low

  [ Sergei Golovan ]
  * Added versioned dependency on erlang.
  * Added workaround for FTBFS caused by lyx bug #381622
    (Closes: #381934).

 -- Erlang Packagers <erlang-pkg-devel@lists.berlios.de>  Wed, 09 Aug 2006 11:42:10 +0400

manderlbot (0.9.2-5) unstable; urgency=low

  [ Sergei Golovan ]
  * Moved dependencies which are used only for architecture
    independent build part from Build-Depends to Build-Depends-Indep
    in debian/control file.

 -- Erlang Packagers <erlang-pkg-devel@lists.berlios.de>  Tue, 04 Jul 2006 15:27:01 +0400

manderlbot (0.9.2-4) unstable; urgency=low

  * New maintainers (Closes:  #342918).

  [ Sergei Golovan ]
  * Changed Build-Depends to erlang 1:10.b.10 package structure.
  * Build using xmerl included into erlang distribution. Therefore
    removed libxmerl-erlang dependency (Closes: #337419).
  * Removed latin1 characters from default config file. Otherwise
    xmerl_scan:file throws an error about invalid UTF-8 symbols.
    So, config should be in UTF-8 encoding (or encoding should be
    specified explicitly).
  * Removed useless xmerl headers from binary package.
  * Removed Postscript doc (leaving PDF).
  * Closes: #340040 since lyx is in Build-Depends.
  * Incorporated changes made in NMU bug (Closes: #242043).
  * Bumped standards version to 3.7.2.

  [ Torsten Werner ]
  * fixed 3 lintian errors (Build-Depends, targets in debian/rules and
    debian/compat)

 -- Torsten Werner <twerner@debian.org>  Sat, 24 Jun 2006 22:50:19 +0200

manderlbot (0.9.2-3.1) unstable; urgency=low

  * NMU
  * run erl with -noshell during build (Closes: #242043)

 -- Goswin von Brederlow <brederlo@informatik.uni-tuebingen.de>  Sun, 18 Apr 2004 23:19:31 +0200

manderlbot (0.9.2-3) unstable; urgency=low

  * No need of gs-common 

 -- Julien Danjou <acid@debian.org>  Sun, 29 Feb 2004 15:28:31 +0100

manderlbot (0.9.2-2) unstable; urgency=low

  * Added a dep against tetex-bin and gs-common (Closes: #232980)
  * Added ps and pdf doc 

 -- Julien Danjou <acid@debian.org>  Sun, 29 Feb 2004 15:25:17 +0100

manderlbot (0.9.2-1) unstable; urgency=low

  * New upstream release 

 -- Julien Danjou <acid@debian.org>  Fri, 12 Dec 2003 14:39:54 +0100

manderlbot (0.9.1-1) unstable; urgency=low

  * New upstream release

 -- Julien Danjou <acid@debian.org>  Tue, 26 Aug 2003 16:48:40 +0200

manderlbot (0.9.0-1) unstable; urgency=low

  * New upstream release
  * License is now GPL with exception (Closes: #193571)
  * FTBFS fixed (Closes: #191897)

 -- Julien Danjou <acid@debian.org>  Fri, 22 Aug 2003 11:35:01 +0200

manderlbot (0.8.2-1) unstable; urgency=low

  * Initial Release. (Closes: #178047)

 -- Julien Danjou <acid@debian.org>  Thu, 23 Jan 2003 16:14:07 +0100

